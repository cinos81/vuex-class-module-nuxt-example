export const api = {
  async fetch() {
    return [
      [
        '10-SOCKS',
        {
          name: 'blue socks',
          price: 1.99
        }
      ],
      [
        '20-SOCKS',
        {
          name: 'red tube socks',
          price: 2.99
        }
      ],
      [
        '30-SOCKS',
        {
          name: 'ankle socks',
          price: 1.0
        }
      ],
      [
        '10-SHOES',
        {
          name: 'blue suede',
          price: 110.99
        }
      ],
      [
        '20-SHOES',
        {
          name: 'red converse all stars',
          price: 72.99
        }
      ],
      [
        '30-SHOES',
        {
          name: 'zip up ankle boot',
          price: 89.99
        }
      ]
    ];
  }
}
