import { Module, VuexModule, Mutation, Action, MutationAction } from 'vuex-module-decorators'
import { api } from './api';

@Module({
  namespaced: true,
  stateFactory: true,
  name: 'Shop'
})
export default class Shop extends VuexModule {
  _cartItems = []
  _products = new Map()
  _filter = 'SHOES'

  @MutationAction
  async init() {
    const data = await api.fetch();
    return {
      _products: new Map(data)
    };
  }

  get cartItems() {
    return this._cartItems;
  }

  get products() {
    return this._products;
  }

  get filter() {
    return this._filter;
  }

  @Mutation
  setFilter(_value) {
    this._filter = _value;
  }

  @Mutation
  addItemToCart(_product) {
    // this.commit('setItemToCart', _value);
    this._cartItems.push({
      id: _product.id,
      ..._product
    });
  }

  @Action
  async removeItemFromCart(index) {
    this._cartItems.splice(index, 1);
    // this.commit('removeItemFromCart', index)
    return this._cartItems;
  }

  @Action
  async clearCart(_product) {
    // this.commit('setCart', _product)
    this._cartItems = [];
  }

  get cartSize() {
    return this._cartItems.length;
  }

  get currentFilter() {
    return this._filter;
  }

  get cartTotal() {
    return this._cartItems.reduce((total, item) => {
      return total + item.price;
    }, 0.0);
  }

  get filteredProducts() {
    return (filter) => {
      let result = [];
      this.products.forEach((v, k) => {
        if (k.indexOf(filter) !== -1 || filter === "SHOW_ALL") {
          result.push({ ...v, id: k });
        }
      });
      return result;
    }
  }
}
